FROM docker

ENV CODECLIMATE_IMAGE="codeclimate/codeclimate:0.85.23"

COPY run.sh /
COPY codeclimate_defaults /codeclimate_defaults

RUN apk add --no-cache jq

ENTRYPOINT  ["/run.sh"]

CMD ["--help"]
